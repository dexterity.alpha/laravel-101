

@extends ('layout')

@section ('title')
	
	<title> Task List</title>
@endsection

@section('content')
	<ul>
		@foreach( $tasks as $task)
			<li>
				<h4>
				 <a href="/tasks/{{ $task->id }}"> {{ $task->body }} </a> 
				</h4>
			</li>
		@endforeach
	</ul>
@endsection


@section ('footer')
		
		<footer>tasks footer </footer>

@endsection