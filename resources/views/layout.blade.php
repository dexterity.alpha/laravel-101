<!DOCTYPE html>
<html>
<head>
	@yield('title')
	@yield('styles')

</head>
<body>

	@yield('content')

	@yield('footer')
</body>
</html>